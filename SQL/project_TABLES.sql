CREATE SCHEMA if not exists project;

CREATE TABLE project.company (
company_id INT(3) NOT NULL AUTO_INCREMENT, 
company_name varchar(30), 
company_email varchar(30),
company_tel BIGINT(15),
main_user_name varchar(30)  ,
CONSTRAINT PK_company PRIMARY KEY (company_id)
);

CREATE TABLE project.employee (
employee_id INT(3) NOT NULL AUTO_INCREMENT, 
employee_firstname varchar(50) NOT NULL, 
employee_lastname varchar(50) , 
job varchar(15),
email varchar(20),
telephone BIGINT(15),
company_id INT(3),
CONSTRAINT PK_employee PRIMARY KEY (employee_id),
CONSTRAINT FK_employee_company_company_id FOREIGN KEY (company_id)
	REFERENCES project.company(company_id)
);


INSERT INTO project.company(company_name,
	company_email, company_tel, main_user_name)
	VALUES('BCS', 'BCS@eesti.ee', +372123456789, 'Uuno');
    
INSERT INTO project.company(company_name,
	company_email, company_tel, main_user_name)
	VALUES('BCS2', 'BCS2@eesti.ee', +372123456789, 'Uuno2');
    
INSERT INTO project.company(company_name,
	company_email, company_tel, main_user_name)
	VALUES('BCS3', 'BCS3@eesti.ee', +372123456789, 'Uuno3');
    
INSERT INTO project.employee(employee_firstname, employee_lastname,
	job, email, telephone, company_id)

	VALUES('Ats', 'Kuusk', 'Kokk', 'Ats@eesti.ee', 123456789, (Select company_id from company 
		where company_name = 'BCS'));
    
INSERT INTO project.employee(employee_firstname, employee_lastname,
	job, email, telephone, company_id)

	VALUES('Ats2', 'Kuusk2', 'Kokk2', 'Ats2@eesti.ee', 123456789, (Select company_id from company 
		where company_name = 'BCS2'));
    
INSERT INTO project.employee(employee_firstname, employee_lastname,
	job, email, telephone,company_id )

	VALUES('Ats3', 'Kuusk3', 'Kokk3', 'Ats3@eesti.ee', 123456789, (Select company_id from company 
		where company_name = 'BCS3'));



