var mysql = require('mysql');
var connection  = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: 'Parool11',
	database: 'project',
});

connection.connect(function(err){
if(!err) {
    console.log("Database is connected ... nn");
} else {
    console.log("Error connecting database ... nn");
}});

exports.getUserByEmail = function(userEmail, callback) {
    connection.query('SELECT * FROM employee WHERE employee_email = ?',[userEmail], function (err, rows, fields) {
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing query');
        }
})};

exports.register = function(req,res){
  var users={
    "employee_firstname":req.body.employee_firstname,
    "employee_lastname":req.body.employee_lastname,
    "employee_email":req.body.employee_email,
    "employee_password":req.body.employee_password,
  }
  connection.query('INSERT INTO employee SET ?',users, function (error, results, fields) {
  if (error) {
    console.log("error ocurred",error);
    res.send({
      "code":400,
      "failed":"error ocurred"
    })
  }else{
    console.log('The solution is: ', results);
    res.send({
      "code":200,
      "success":"user registered sucessfully"
        });
}})};

exports.login = function(req,res){
  var email= req.body.employee_email;  
  var password = req.body.employee_password;
  connection.query('SELECT * FROM employee WHERE employee_email = ?',[email], function (error, results, fields) {
  if (error) {
    // console.log("error ocurred",error);
    res.send({
      "code":400,
      "failed":"error ocurred"
    })
  }else{     
    if(results.length >0){
      if(results[0].employee_password == password){
		  console.log(email);
		  var userId= results[0].employee_id;
		  connection.query('SELECT roles.* FROM project.user_role INNER JOIN project.roles on user_role.role_id = roles.role_id WHERE user_id = ?',[userId], function (errorRoles, resultsRoles, fieldsRoles) {
			for(var roleIndex in resultsRoles) {
				if(resultsRoles[roleIndex].role_name == "admin") {
					req.session.admin = true;
					req.session.user = email;						
						res.send({
							"role":"admin",
							"userName":results[0].employee_firstname +" "+ results[0].employee_lastname,
							"userId":results[0].employee_id,
							"code":200,
							"success":"login sucessfull"
							});					
				}
				if(resultsRoles[roleIndex].role_name == "company") {
					req.session.company = true;
					req.session.user = email;						
						res.send({
							"role":"company",
							"userName":results[0].employee_firstname +" "+ results[0].employee_lastname,
							"userId":results[0].employee_id,
							"code":201,
							"success":"login sucessfull"
							});					
				}
				if(resultsRoles[roleIndex].role_name == "user") {
					req.session.user = true;
					req.session.user = email;
						res.send({
							"role":"user",
							"userName":results[0].employee_firstname +" "+ results[0].employee_lastname,
							"userId":results[0].employee_id,
							"code":202,
							"success":"login sucessfull"
							});
				}
			}			
		  })		
      }
      else{
        res.send({
          "code":204,
          "success":"Email and password does not match"
            });
      }
    }
    else{
      res.send({
        "code":204,
        "success":"Email does not exits"
          });
    }
  }
  });
}

exports.passwordCheck = function(employee, callback){
  var id = employee.employee_id;  
  var password = employee.employee_password;
  connection.query('SELECT employee_password FROM employee WHERE employee_id = ?',[id], function (error, results, fields) {
	if(!error){
		if(password == results[0].employee_password) {
			return callback(results);
		} else{
			console.log("was wrong pw in loginroutes");
			var wrongPasswordErr = new Error();
			wrongPasswordErr.name = "WrongPasswordError";
			wrongPasswordErr.message = "Old password incorrect!";
			wrongPasswordErr.code = 205;
			return callback(wrongPasswordErr);
		}
		
	} else {
		console.log("error ocurred");
	}
})}

exports.logout = function (req, res) {
  req.session.destroy();
  res.send({
        "code":200,
        "success":"logout success!"
          });
};

