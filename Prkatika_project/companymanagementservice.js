var dbApis = require('./companymanagement');
var dbApis2 = require('./employeemanagement');
var dbApis3 = require('./trainingmanagement');
var testapi = require('./testmanagement');
var login = require('./loginroutes');
var csv = require("csvtojson");
var fs = require("fs");

var express = require('express');
var session = require('express-session');
var fileUpload = require('express-fileupload');
var app = express();
var morgan = require('morgan');
var bodyParser = require('body-parser');
var passport = require('passport');
var cookieParser = require('cookie-parser');
var session = require('express-session');

var hostname = 'localhost';
var port = 3000;

var router = express.Router();
var app = express();

app.use(session({
    secret: '2C44-4D44-WppQ38S',
    resave: true,
    saveUninitialized: true
}));

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

var authAdmin = function(req, res, next) {
    var user="";
    login.getUserByEmail(req.session.user, function(userResponse){
        if (userResponse.length == 1 && userResponse[0].employee_email && req.session && req.session.admin) {
            return next();
        }else {
            return res.sendStatus(401);
        }
    });    
};

var authCompanyUser = function(req, res, next) {
    var user="";
    login.getUserByEmail(req.session.user, function(userResponse){
        if (userResponse.length == 1 && userResponse[0].employee_email && req.session && req.session.company || req.session.admin) {
            return next();
        }else {
            return res.sendStatus(401);
        }
    });    
};

var authUser = function(req, res, next) {
    var user="";
    login.getUserByEmail(req.session.user, function(userResponse){
        if (userResponse.length == 1 && userResponse[0].employee_email && req.session && req.session.user || req.session.company || req.session.admin) {
            return next();
        }else {
            return res.sendStatus(401);
        }
    });    
};

app.get('/god.html', authAdmin);
app.get('/workers.html', authCompanyUser);
app.get('/trainings.html', authCompanyUser);
app.get('/test.html', authCompanyUser);
app.get('/statistics.html', authAdmin);
app.get('/forWorkers.html', authUser);

router.post('/register',login.register);
router.post('/login',login.login);
router.post('/logout',login.logout);
app.use('/api', router);

app.use(fileUpload());
app.all('/api/company', function(req, res, next){
	res.writeHead(200, {'Content-Type' : 'application/json'});
	next();
});

app.get('/api/company', function(req, res, next){
	dbApis.getAllCompanys(function(companyResponse){
		console.log(companyResponse);
		res.end(JSON.stringify(companyResponse));
})});

app.post('/api/company', function(req, res, next){	
	dbApis.addNewCompany(req.body.company_name, req.body.company_email, req.body.company_tel, req.body.main_user_name, function(companyResponse){
		console.log(companyResponse);
		res.end(JSON.stringify(companyResponse));
})});

app.delete('/api/company/:companyId', function(req, res, next){
		dbApis.deleteCompanyById(req.params.companyId,function(companyResponse){
		console.log(companyResponse);
		res.end('{"company_id":'+ req.params.companyId + '}');
})});

app.put('/api/company', function(req, res, next){    
    dbApis.updateCompany(req.body, function(companyResponse){
        console.log(companyResponse);
        res.end(JSON.stringify(req.body));
})});

/* Employees */

app.all('/api/employee', function(req, res, next){
	res.writeHead(200, {'Content-Type' : 'application/json'});
	next();
});

app.get('/api/employee', function(req, res, next){
	dbApis2.getAllEmployees(function(employeeResponse){
		console.log(employeeResponse);
		res.end(JSON.stringify(employeeResponse));
})});

app.get('/api/employee/:employeeId', function(req, res, next){
	dbApis2.getEmployeeById(req.params.employeeId,function(employeeResponse){
		console.log(employeeResponse);
		res.end(JSON.stringify(employeeResponse));
})});

app.post('/api/employee', function(req, res, next){
	dbApis2.addNewEmployee(req.body.employee_firstname, req.body.employee_lastname,req.body.job, req.body.employee_email, req.body.employee_tel,req.body.company_id, function(employeeResponse){
		console.log(employeeResponse);
		res.end(JSON.stringify(employeeResponse));
})});

app.delete('/api/employee/:employeeId', function(req, res, next){
	dbApis2.deleteEmployeeById(req.params.employeeId,function(employeeResponse){
		console.log(employeeResponse);
		res.end('{"employee_id":'+ req.params.employeeId + '}');
})});

app.put('/api/employee', function(req, res, next){    
    dbApis2.updateEmployee(req.body, function(employeeResponse){
        console.log(employeeResponse);
        res.end(JSON.stringify(req.body));
})});

app.put('/api/employeecontactinfo', function(req, res, next){    
    dbApis2.updateEmployeeDetail(req.body, function(employeeResponse){
        console.log(employeeResponse);
        res.end(JSON.stringify(req.body));
})});

app.put('/api/employeepasswordcheck', function(req, res, next){    
    login.passwordCheck(req.body, function(employeeResponse){
        console.log("passwordCheck response:");
		console.log(employeeResponse);
        res.end(JSON.stringify(employeeResponse));
})});

app.put('/api/employeepassword', function(req, res, next){    
    dbApis2.updateEmployeePassword(req.body, function(employeeResponse){
        console.log(employeeResponse);
        res.end(JSON.stringify(req.body));
})});

/*MassImport*/
app.post('/massImport', function(req, res, next){
  console.log(req.body);
  csv({
      delimiter: ','
    })
  .fromString(req.body.message)
  .on('json', (employee)=>{
    console.log(employee);
      dbApis2.addNewEmployee(employee.employee_firstname,employee.employee_lastname, employee.job, employee.employee_email, employee.employee_tel, employee.company_id, function(employeeResponse){
          console.log(employeeResponse);
          res.end(JSON.stringify(req.body));   
		  });
  })
  .on('done', (error) => {
    console.log('end csv to json');
    return res.status(500).send(error);
  })
  res.location('/static/workers.html');
  res.status(200).send(null);
});

/* Trainings */
app.all('/api/trainings', function(req, res, next){
	res.writeHead(200, {'Content-Type' : 'application/json'});
	next();
});

app.get('/api/trainings', function(req, res, next){
	dbApis3.getAllTrainings(function(trainingsResponse){
		console.log(trainingsResponse);
		res.end(JSON.stringify(trainingsResponse));
})});

app.post('/api/trainings', function(req, res, next){
	dbApis3.addNewTraining(req.body.training_name, req.body.training_info, req.body.training_duration, req.body.training_person_cost, function(trainingsResponse){
		console.log(trainingsResponse);
		res.end(JSON.stringify(trainingsResponse));
})});

app.delete('/api/trainings/:trainingId', function(req, res, next){
	dbApis3.deleteTrainingById(req.params.trainingId,function(trainingsResponse){
		console.log(trainingsResponse);
		res.end('{"training_id":'+ req.params.trainingId + '}');
})});

app.put('/api/trainings', function(req, res, next){    
    dbApis3.updateTraining(req.body, function(trainingsResponse){
        console.log(trainingsResponse);
        res.end(JSON.stringify(req.body));
})});

/* Tests */
app.all('/api/test', function(req, res, next){
	res.writeHead(200, {'Content-Type' : 'application/json'});
	next();
});

app.get('/api/test', function(req, res, next){
	testapi.getAllTests(function(testResponse){
		console.log(testResponse);
		res.end(JSON.stringify(testResponse));
})});

app.post('/api/test', function(req, res, next){
	testapi.addNewTest(req.body.test_name, req.body.test_desc, req.body.test_training, req.body.test_exp, function(testResponse){
		console.log(testResponse);
		res.end(JSON.stringify(testResponse));
})});

app.delete('/api/test/:testId', function(req, res, next){
	testapi.deleteTestById(req.params.testId,function(testResponse){
		console.log(testResponse);
		res.end('{"test_id":'+ req.params.testId + '}');
})});

app.put('/api/test', function(req, res, next){    
    testapi.updateTest(req.body, function(testResponse){
        console.log(testResponse);
        res.end(JSON.stringify(req.body));
})});

/* Completed Trainings */
app.get('/api/employee/training/:employeeId', function(req, res, next){console.log("12");
	dbApis2.getEmployeeTrainings(req.params.employeeId, function(completedTrainingResponse){
		console.log(completedTrainingResponse); 		

		res.end(JSON.stringify(completedTrainingResponse));
})});

app.get('/api/employee/completedtraining/:employeeId', function(req, res, next){console.log("12");
	dbApis2.getCompletedTrainingsTable(req.params.employeeId, function(completedTrainingResponse){
		console.log(completedTrainingResponse); 		

		res.end(JSON.stringify(completedTrainingResponse));
})});		

app.use(express.static(__dirname + '/public'));

app.listen(port, hostname, function(){
	console.log(`Server is running at http//${hostname}:${port}/`)
});