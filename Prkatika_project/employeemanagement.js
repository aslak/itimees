var mysql = require('mysql');
var connection  = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: 'Parool11',
	database: 'project',
});

connection.connect();

exports.addNewEmployee = function(firstName,lastName,job,employeeEmail ,employeeTelephone,companyId, callback){
connection.query("INSERT INTO employee(employee_firstname, employee_lastname, job, employee_email, employee_tel, company_id, employee_password) VALUES('" + firstName +"','" + lastName +"','" + job +"','" + employeeEmail +"','" + employeeTelephone +"','" + companyId +"','Password123')", function(err, rows, field){
	if(!err){
		callback(rows);
	}else{
		console.log('Error on performing queryafdh');
}})};

exports.getAllEmployees= function(callback){
	connection.query('SELECT * FROM employee', function(err, rows, field){
	if(!err){
		return callback(rows);
	}else{
		console.log('Error on performing query');
}})};

exports.getEmployeeById= function(employeeId, callback){
	connection.query('SELECT * FROM employee WHERE employee_id = ' +employeeId, function(err, rows, field){
	if(!err){
		return callback(rows);
	}else{
		console.log('Error selecting on performing employee by ID');
}})};

exports.deleteEmployeeById= function(employeeId, callback){
connection.query('DELETE FROM employee WHERE employee_id = ' +employeeId, function(err, rows, field){
	if(!err){
		return callback(rows);
	}else{
		console.log('Error on performing delete');
}})};

exports.updateEmployee = function(employee, callback) {
    connection.query("UPDATE employee SET employee_firstname = '" + employee.employee_firstname + "', employee_lastname = '" + employee.employee_lastname + "' ,job = '" + employee.job + "', employee_email = '" + employee.employee_email + "', employee_tel = '" + employee.employee_tel + "' WHERE employee_id = " + employee.employee_id, function(err, rows, fields){
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing update query');
}})};

exports.updateEmployeeDetail = function(employee, callback) {
	console.log("UPDATE employee SET employee_email = '" + employee.employee_email + "', employee_tel = '" + employee.employee_tel + "' WHERE employee_id = " + employee.employee_id);
    connection.query("UPDATE employee SET employee_email = '" + employee.employee_email + "', employee_tel = '" + employee.employee_tel + "' WHERE employee_id = " + employee.employee_id, function(err, rows, fields){
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing update detail query');
}})};

exports.updateEmployeePassword = function(employee, callback) {
	console.log("UPDATE employee SET employee_password = '" + employee.employee_password + "' WHERE employee_id = " + employee.employee_id);
    connection.query("UPDATE employee SET employee_password = '" + employee.employee_password + "' WHERE employee_id = " + employee.employee_id, function(err, rows, fields){
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing update password detail query');
}})};

exports.getEmployeeTrainings= function(employeeId, callback) { console.log("123");
	connection.query('SELECT employee.employee_firstname, employee.employee_lastname, trainings.training_name, '+
		'employee.employee_id, completed_trainings.training_id FROM completed_trainings INNER JOIN employee ON '+
		'completed_trainings.employee_id =employee.employee_id INNER JOIN trainings ON completed_trainings.training_id=trainings.training_id  '+
		'WHERE completed_trainings.employee_id =?',[employeeId], function(err, rows, field){
		if(!err){
			return callback(rows);
					
		}else{
			console.log('Error on completed trainings employeeid'+employeeId);
}})};

exports.getCompletedTrainingsTable= function(employeeId, callback) { console.log("123");
	connection.query('SELECT t.* FROM project.completed_trainings ct inner join trainings t on ct.training_id = t.training_id WHERE ct.employee_id =?',[employeeId], function(err, rows, field){
		if(!err){
			return callback(rows);
					
		}else{
			console.log('Error on completed trainings employeeid'+employeeId);
}})};

/*
exports.getTrainingEmployees= function(trainingId, callback) {console.log("321");
	connection.query('SELECT employee_firstname, employee_lastname, training_name  FROM project.completed_trainings INNER JOIN project.employee ON completed_trainings.employee_id =employee.employee_id INNER JOIN project.trainings ON completed_trainings.training_id=trainings.training_id  WHERE training_id = '+trainingId, function(err, rows, field){
		if(!err){
			return callback(rows);
		}else{
			console.log('Error on completed trainings trainingid');
}})};
*/
